#!/usr/bin/env bash

pkg_name=messenger-auto-reply
install_dir=/opt/${pkg_name}

install -d "${install_dir}"
cp -r node_modules "${install_dir}/"
cp -r src "${install_dir}/"
install -d "${install_dir}/config"
install -m644 "config/system.json" "${install_dir}/config/default.json"
install -m755 "${pkg_name}.js" "${install_dir}/"

ln -s "${install_dir}/${pkg_name}.js" "/usr/bin/${pkg_name}"

useradd --system -m -b /var/lib -s /sbin/nologin facebook
install -m640 -o facebook -g facebook "state/default-${pkg_name}-state.json" "/var/lib/facebook/${pkg_name}-state.json"

install -m644 ${pkg_name}.service /usr/lib/systemd/system/

# vim: set noexpandtab:
