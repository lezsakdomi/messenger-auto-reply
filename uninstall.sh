#!/usr/bin/env bash

pkg_name=messenger-auto-reply
install_dir=/opt/${pkg_name}

rm -rf "${install_dir}" "/usr/lib/systemd/system/${pkg_name}"
